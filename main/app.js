function addTokens(input, tokens){
    if (typeof input !== "string") {
        throw new Error("Invalid input");
    }
    if (input.length < 6) {
        throw new Error("Input should have at least 6 characters");
    }
    if (!Array.isArray(tokens)) {
        throw new Error("Tokens should be an array");
    }
    if (!('tokenName' in tokens[0]) || typeof tokens[0].tokenName !== "string") {
        throw new Error("Invalid array format");
    }
    /*if (input.indexOf("...") === -1) {
        return input;
    }*/
    for (var i = 0; i < (input.match(/\.\.\./g) || []).length; i++) {
       // input = input.replace("...", tokens[i].tokenName);
       var res = "${" + tokens[i].tokenName + "}";
       input = input.replace("...", res);
    }
    return input;
}

const app = {
    addTokens: addTokens
}

module.exports = app;

/* 
 * Testul numarul 5 sustine ca rezultatul returnat
 * ar trebui sa fie 'Subsemnatul ${subsemnatul}'.
 * In cazul inlocuirii tokenului, rezultatul ar trebui
 * sa fie, de fapt, 'Subsemnatul subsemnatul'. Aceasta
 * functionalitate a fost implementata la linia 18.
 *
 * Cu toate acestea, am realizat o modificare
 * la liniile 19-20 astfel incat testul sa functioneze.
 */